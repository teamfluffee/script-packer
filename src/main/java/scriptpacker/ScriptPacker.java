package scriptpacker;

import org.objectweb.asm.ClassReader;
import scriptpacker.utilities.Utils;
import scriptpacker.visitors.DependencyVisitor;
import scriptpacker.visitors.ScriptManifestVisitor;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@SuppressWarnings("unused")
public class ScriptPacker {

    /**
     * Packs a single script into a zip file.
     *
     * @param scriptName Name of script to pack into a zip file
     * @param outputDirectory File representing the output directory for the packed script
     * @param scriptFile File containing the class that is the base of the script (the class that TRiBot executes first)
     * @param sourceFiles List<File> List of all source files for scripts on your PC.
     * @throws IOException Thrown if there are any issues accessing the necessary source and class files.
     */
    public void packScript(String scriptName, File outputDirectory, File scriptFile, List<File> sourceFiles) throws IOException {
        DependencyVisitor dependencyVisitor = new DependencyVisitor();
        ScriptManifestVisitor manifestVisitor = new ScriptManifestVisitor();
        ClassReader classReader = new ClassReader(new FileInputStream(scriptFile));

        classReader.accept(manifestVisitor, 0);
        classReader.accept(dependencyVisitor, 0);
        //The accept function actually has side effects which load classes into the manifest and dependency visitor
        //This is why they're not initialized but we still use them.

        //Another side effects function. As we visit dependencies the classes get added to the dependency visitor's
        //internal class set. So we find all the dependencies using the object and then read from the object
        findAllSubDependencies(dependencyVisitor);
        Set dependencies = dependencyVisitor.getDependencyClasses();

        File zipFile = getZipFile(outputDirectory, scriptName);
        if (packZip(getScriptFiles(sourceFiles, dependencies), zipFile)) {
            JOptionPane.showMessageDialog(null, "Successfully packed " + zipFile.getName() +
                    "\nDirectory: " + zipFile.getParentFile().getAbsolutePath());
        }
    }

    /**
     * Visits the dependencies of a script using the passed dependency visitor.
     *
     * @param dependencyVisitor DependencyVisitor object which is set up to visit the script's classes
     * @throws IOException
     */
    private void findAllSubDependencies(DependencyVisitor dependencyVisitor) throws IOException {
        findAllSubDependencies(dependencyVisitor, new HashSet<>());
    }

    /**
     * Visits the dependencies of a script using the passed dependency visitor.
     *
     * @param dependencyVisitor DependencyVisitor object which is set up to visit the script's classes
     * @param checkedClasses Set<String> Set of strings containing the names of the all the classes we've checked. If
     *                       we've checked a class we know if it is, or is not a dependency of the script.
     * @throws IOException
     */
    private void findAllSubDependencies(DependencyVisitor dependencyVisitor, Set<String> checkedClasses) throws IOException {
        Iterator depdencyIterator = (new HashSet(dependencyVisitor.getDependencyClasses())).iterator();

        while (depdencyIterator.hasNext()) { //Iterate over all dependencyName classes
            String dependencyName = (String) depdencyIterator.next();
            if (dependencyName.startsWith("scripts/") && !checkedClasses.contains(dependencyName)) {
                if (dependencyName.charAt(dependencyName.length() - 5) == '.' ||
                    dependencyName.charAt(dependencyName.length() - 4) == '.') {
                    continue;
                }
                ClassReader cr = new ClassReader(new FileInputStream(new File(Utils.getBinDirectory(),
                        dependencyName + ".class")));
                cr.accept(dependencyVisitor, 0);
                checkedClasses.add(dependencyName);
                //If it's a new dependencyName we need to look for it's dependencies too, hence recursion.
                findAllSubDependencies(dependencyVisitor, checkedClasses);
            }
        }
    }

    /**
     * Gets the script's source files by iterating over all dependencies of the script and finding the corresponding
     * files in the list of source files
     *
     * @param sourceFiles List<File> List of all source files on your local PC
     * @param dependencies Set<String> Set containing the names of all the dependencies of the script
     * @return List<File> containing the source files for the script
     */
    private List<File> getScriptFiles(List<File> sourceFiles, Set<String> dependencies) {
        ArrayList<File> sourceFilesToPack = new ArrayList<>();
        ArrayList<String> missingFiles = new ArrayList();
        for (String dependency : dependencies) {
            //If file is not in your scripts folder, and is not a primary class.
            if (!dependency.startsWith("scripts/") || dependency.indexOf('$') != -1) {
                continue;
            }
            dependency = File.separator.equals("\\") ? dependency.replaceAll("/", "\\\\") : dependency;
            if (dependency.charAt(dependency.length() - 5) != '.' && dependency.charAt(dependency.length() - 4) != '.') {
                dependency = dependency + ".java";
            }
            boolean found = false;
            for (File sourceFile : sourceFiles) {
                if (sourceFile.getPath().endsWith(dependency)) {
                    found = true;
                    sourceFilesToPack.add(sourceFile);
                    break;
                }
            }
            if (!found) {
                missingFiles.add(dependency);
            }
        }
        if (missingFiles.size() > 0) {
            StringBuilder sb = new StringBuilder();
            missingFiles.forEach(missingFile -> {
                        sb.append(missingFile);
                        sb.append("\n");
                    }
            );
            System.out.println("Can't find the following source files:\n" + sb.toString());
        }
        return sourceFilesToPack;
    }

    /**
     * Packs all the files passed into a single zip file.
     *
     * @param files List<File> List of files to pack into the zip
     * @param zipFile File representing the zip to pack the files into
     * @return True if the packing was successful, false otherwise
     */
    private boolean packZip(List<File> files, File zipFile) {
        try {
            if (!zipFile.getParentFile().exists()) { //Make the parent directories if the parents don't exist
                zipFile.mkdirs();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
            for (File file : files) { //For each file in the list
                FileInputStream fis = new FileInputStream(file);
                String filePath = file.getAbsolutePath();
                filePath = filePath.substring(filePath.lastIndexOf(File.separator + "scripts" + File.separator) + File.separator.length());
                ZipEntry zipEntry = new ZipEntry(filePath);
                zipOutputStream.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int length = fis.read(bytes);
                while (length >= 0) { //While we have bytes left to read from the file
                    zipOutputStream.write(bytes, 0, length); //Write those bytes to the file
                    length = fis.read(bytes);
                }
                zipOutputStream.closeEntry();
                fis.close();
            }
            zipOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Gets the zip file to generate. Automatically appends a number if original exists.
     *
     * @param outputDirectory Directory to create zip in
     * @param scriptName Name of script to create zip of
     * @return File where the zip file will be created.
     */
    private File getZipFile(File outputDirectory, String scriptName) {
        File zipFile = new File(outputDirectory,scriptName + ".zip");
        int version = 1;
        while (zipFile.exists()) {
            zipFile = new File(zipFile.getParent(), scriptName + String.format("(%d).zip", version));
            version++;
        }
        return zipFile;
    }

}

package scriptpacker.visitors;

import org.objectweb.asm.*;
import org.objectweb.asm.signature.SignatureReader;

import java.util.HashSet;

/**
 * Visits a dependency class of a script
 */
public class DependencyVisitor extends ClassVisitor {

    //Names of all classes that are dependencies
    private final HashSet dependencyClasses = new HashSet();

    public DependencyVisitor() {
        super(Opcodes.ASM9);
    }

    public void addDesc(String description) {
        this.addType(Type.getType(description));
    }

    public void addMethodDesc(String description) {
        this.addType(Type.getReturnType(description));
        Type[] argumentTypes = Type.getArgumentTypes(description);
        for (int i = 0; i < argumentTypes.length; ++i) {
            Type type = argumentTypes[i];
            this.addType(type);
        }
    }

    public void addName(String name) {
        if (name != null) {
            if (name.startsWith("[L") && name.endsWith(";")) {
                name = name.substring(2, name.length() - 1);
            }
            int index = name.indexOf('$');
            if (index != -1) {
                this.dependencyClasses.add(name);
                name = name.substring(0, index);
            }
            this.dependencyClasses.add(name);
        }
    }

    protected void addNames(String[] names) {
        if (names != null) {
            for (String name : names) {
                this.addName(name);
            }
        }
    }

    protected void addSignature(String signature) {
        (new SignatureReader(signature)).accept(new SignatureDependencyVisitor(this));
    }

    public void addType(Type type) {
        switch (type.getSort()) {
            case Type.ARRAY:
                this.addType(type.getElementType());
                break;
            case Type.OBJECT:
                this.addName(type.getClassName().replace('.', '/'));
        }
    }

    public void addTypeSignature(String signature) {
        (new SignatureReader(signature)).acceptType(new SignatureDependencyVisitor(this));
    }

    public HashSet getDependencyClasses() {
        return this.dependencyClasses;
    }

    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        if (signature == null) {
            this.addName(superName);
            this.addNames(interfaces);
        } else {
            this.addSignature(signature);
        }
    }

    public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
        if (signature == null) {
            this.addDesc(desc);
        } else {
            this.addTypeSignature(signature);
        }

        if (value instanceof Type) {
            this.addType((Type) value);
        }

        return new FieldDependencyVisitor(this);
    }

    public void visitInnerClass(String name, String outerName, String innerName, int access) {
        this.addName(name);
    }

    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        if (signature == null) {
            this.addMethodDesc(desc);
        } else {
            this.addSignature(signature);
        }

        this.addNames(exceptions);
        return new MethodDependencyVisitor(this);
    }
}
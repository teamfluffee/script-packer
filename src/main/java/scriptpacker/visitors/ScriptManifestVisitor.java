package scriptpacker.visitors;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScriptManifestVisitor extends ClassVisitor {
    private final Map<String, Object> attributes = new HashMap();

    public ScriptManifestVisitor() {
        super(Opcodes.ASM9);
    }

    public Map<String, Object> getAttributes() {
        return this.attributes;
    }

    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        return !desc.equals("Lorg/tribot/script/ScriptManifest;") ? null : new AnnotationVisitor(Opcodes.ASM9) {
            public void visit(String name, Object value) {
                attributes.put(name, value);
                super.visit(name, value);
            }

            public AnnotationVisitor visitArray(final String atrName) {
                attributes.put(atrName, new ArrayList());
                return new AnnotationVisitor(Opcodes.ASM9) {
                    public void visit(String name, Object value) {
                        ((List) attributes.get(atrName)).add(value);
                    }
                };
            }
        };
    }
}


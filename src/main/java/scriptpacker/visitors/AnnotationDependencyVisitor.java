package scriptpacker.visitors;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class AnnotationDependencyVisitor extends AnnotationVisitor {

    private DependencyVisitor dependencyVisitor;

    public AnnotationDependencyVisitor(DependencyVisitor dependencyVisitor) {
        super(Opcodes.ASM9);
        this.dependencyVisitor = dependencyVisitor;
    }

    public void visit(String name, Object value) {
        if (value instanceof Type) {
            dependencyVisitor.addType((Type) value);
        }
    }

    public AnnotationVisitor visitAnnotation(String name, String desc) {
        dependencyVisitor.addDesc(desc);
        return this;
    }

    public void visitEnum(String name, String desc, String value) {
        dependencyVisitor.addDesc(desc);
    }
}

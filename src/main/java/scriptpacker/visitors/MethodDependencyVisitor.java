package scriptpacker.visitors;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class MethodDependencyVisitor extends MethodVisitor {

    private DependencyVisitor dependencyVisitor;

    public MethodDependencyVisitor(DependencyVisitor dependencyVisitor) {
        super(Opcodes.ASM9);
        this.dependencyVisitor = dependencyVisitor;
    }

    public void visitFieldInsn(int opcode, String owner, String name, String desc) {
        dependencyVisitor.addName(owner);
        dependencyVisitor.addDesc(desc);
    }

    public void visitLdcInsn(Object cast) {
        if (cast instanceof Type) {
            dependencyVisitor.addType((Type) cast);
            return;
        }

        String castString = cast instanceof String ? (String) cast : "";
        try {
            if (castString.isEmpty() || !castString.startsWith("scripts/") ||
                (castString.charAt(castString.length() - 5) != '.' && castString.charAt(castString.length() - 4) != '.'))
            {
                return;
            }
        } catch (IndexOutOfBoundsException exception) {
            return;
        }
        dependencyVisitor.addName(castString);
    }

    public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
        if (signature == null) {
            dependencyVisitor.addDesc(desc);
        } else {
            dependencyVisitor.addTypeSignature(signature);
        }
    }

    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
        dependencyVisitor.addName(owner);
        dependencyVisitor.addMethodDesc(desc);
    }

    public void visitMultiANewArrayInsn(String desc, int dims) {
        dependencyVisitor.addDesc(desc);
    }

    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
        dependencyVisitor.addName(type);
    }

    public void visitTypeInsn(int opcode, String desc) {
        if (desc.charAt(0) == '[') {
            dependencyVisitor.addDesc(desc);
        } else {
            dependencyVisitor.addName(desc);
        }
    }
}

package scriptpacker.visitors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.signature.SignatureVisitor;

public class SignatureDependencyVisitor extends SignatureVisitor {

    private DependencyVisitor dependencyVisitor;

    public SignatureDependencyVisitor(DependencyVisitor dependencyVisitor) {
        super(Opcodes.ASM9);
        this.dependencyVisitor = dependencyVisitor;
    }

    public void visitClassType(String name) {
        dependencyVisitor.addName(name);
    }

    public void visitInnerClassType(String name) {
        dependencyVisitor.addName(name);
    }
}

package scriptpacker.visitors;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Opcodes;

public class FieldDependencyVisitor extends FieldVisitor {

    private DependencyVisitor dependencyVisitor;

    public FieldDependencyVisitor(DependencyVisitor dependencyVisitor) {
        super(Opcodes.ASM9);
        this.dependencyVisitor = dependencyVisitor;
    }

    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        dependencyVisitor.addDesc(desc);
        return new AnnotationDependencyVisitor(dependencyVisitor);
    }
}

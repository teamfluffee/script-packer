package scriptpacker;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static final String PREF_KEY = "fluffees_script_packer";

    public static void main(String[] args) {
        try {
            SwingUtilities.invokeAndWait(PackerGui::new);
        } catch (InterruptedException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }



}

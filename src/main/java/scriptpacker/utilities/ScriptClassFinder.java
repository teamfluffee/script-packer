package scriptpacker.utilities;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import scriptpacker.visitors.ScriptManifestVisitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("unused")
public class ScriptClassFinder {

    private final String TRIBOT_SCRIPT_CLASS = "org/tribot/script/Script",
            TRIBOT_ENUM_SCRIPT_CLASS = "org/tribot/script/EnumScript";
    private List<String> scriptExtensions;

    public ScriptClassFinder() {
        scriptExtensions = new ArrayList<>();
        scriptExtensions.add(TRIBOT_SCRIPT_CLASS);
        scriptExtensions.add(TRIBOT_ENUM_SCRIPT_CLASS);
    }

    /**
     * Goes through all classes in the TRiBot bin folder to determine which ones are TRiBot Scripts.
     *
     * @return HashMap<String, File> HashMap keyed on String names, with the corresponding script files as values.
     * @throws IOException Thrown if there's an issue accessing a file.
     */
    public HashMap<String, File> findScriptClasses(List<File> scriptClassFiles) throws IOException {
        HashMap<String, File> scriptNames = new HashMap<>();
        ClassReader classReader;
        ScriptManifestVisitor scriptManifestVisitor;

        for (File classFile : scriptClassFiles) {
            if (!isScriptClass(classFile)) {
                continue;
            }

            classReader = new ClassReader(new FileInputStream(classFile));
            scriptManifestVisitor = new ScriptManifestVisitor();
            classReader.accept(scriptManifestVisitor, 0);
            scriptNames.put(getScriptName(scriptManifestVisitor, classFile), classFile);
        }
        return scriptNames;
    }

    /**
     * Gets the name of the script to key it in the hashmap
     *
     * @param scriptManifestVisitor ScriptManifestVisitor that has visited the script's manifest
     * @param classFile File representing the main class file of the script
     * @return String containing the name of the script to display
     */
    private String getScriptName(ScriptManifestVisitor scriptManifestVisitor, File classFile) {
        String scriptName = classFile.getName().substring(0, classFile.getName().length() - 6);
        if (scriptManifestVisitor.getAttributes().get("name") != null) {
            scriptName = (String) scriptManifestVisitor.getAttributes().get("name");
        }
        if (scriptManifestVisitor.getAttributes().get("version") != null) {
            scriptName += " - Version: " + scriptManifestVisitor.getAttributes().get("version");
        }
        return scriptName;
    }

    /**
     * Determines if the class file passed extends Script.
     *
     * @param superClassFile File containing the super class file of the script
     * @return True if the class is an extended script class, false otherwise
     * @throws IOException
     */
    public boolean isExtendedScriptClass(File superClassFile) throws IOException {
        ClassReader classReader = new ClassReader(new FileInputStream(superClassFile));
        String superName = classReader.getSuperName();

        if (superName == null || !isInterfaceOrAbstract(classReader)) {
            return false;
        } else if (scriptExtensions.contains(superName) && !scriptExtensions.contains(classReader.getClassName())) {
            scriptExtensions.add(classReader.getClassName());
            return true;
        } else if (superName.startsWith("scripts/")) {
            superName = File.separator.equals("\\") ? superName.replaceAll("/", "\\\\") : superName;
            return isExtendedScriptClass(
                    new File(Utils.getBinDirectory() + File.separator + superName + ".class"));
        } else {
            return false;
        }
    }

    /**
     * Determines if the class is a TRiBot Script
     *
     * @param classFile File to check
     * @return True if the class is a TRiBot script, false otherwise.
     * @throws IOException
     */
    public boolean isScriptClass(File classFile) throws IOException {
        ClassReader classReader = new ClassReader(new FileInputStream(classFile));
        String superName = classReader.getSuperName();

        if (superName == null) { //All scripts must extend at least a Script class
            return false;
        } else if (scriptExtensions.contains(superName) && !scriptExtensions.contains(classReader.getClassName())) {
            //If the super class is a known extension and the class is not and interface or
            // abstract class then it must be the script class
            if (!isInterfaceOrAbstract(classReader)) {
                return true;
            }
            scriptExtensions.add(classReader.getClassName());
            return false;
        } else if (superName.startsWith("scripts/")){
            superName = File.separator.equals("\\") ? superName.replaceAll("/", "\\\\") : superName;
            return isExtendedScriptClass
                    (new File(Utils.getBinDirectory() + File.separator + superName + ".class"));
        } else {
            return false;
        }
    }

    /**
     * Detects if the classReader's class is an interface or an abstract class
     *
     * @param classReader ClassReader object to check
     * @return True if the class is an interface or an abstract class, false otherwise.
     */
    private boolean isInterfaceOrAbstract(ClassReader classReader) {
        return (classReader.getAccess() & Opcodes.ACC_INTERFACE) != 0 ||
                (classReader.getAccess() & Opcodes.ACC_ABSTRACT) != 0;
    }

}

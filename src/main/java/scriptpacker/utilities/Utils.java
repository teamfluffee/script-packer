package scriptpacker.utilities;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Utils {

    public static File getWorkingDirectory() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.indexOf("win") >= 0) {
            return new File(System.getenv("APPDATA"), ".tribot");
        } else if (osName.indexOf("mac") >= 0) {
            return new File(System.getProperty("user.home") + "/Library/Application Support", "tribot");
        } else {
            return new File(System.getProperty("user.home"), ".tribot");
        }
    }

    public static File getBinDirectory() {
        return new File(getWorkingDirectory(), "bin");
    }

    /**
     * Gets the Path to the FluffeeScripts directory, used for file writing.
     *
     * @return Path to directory as a String.
     */
    public static String getFluffeeScriptsDirectory() {
        return Utils.getWorkingDirectory().getAbsolutePath() + File.separator + "FluffeeScripts" + File.separator;
    }

    /**
     * Creates file given file contents and file name. If file name contains separator characters it creates the necessary subdirectories to satisfy the path.
     *
     * @param fileContents byte[] to write to file
     * @param fileName     String containing file name, or path relative to TRiBot Directory.
     * @return File path as String if successful, null otherwise.
     */
    public static String createFile(byte[] fileContents, String fileName) {
        String directoryPath = createSubdirectories(fileName);
        if (fileName.contains(File.separator))
            fileName = fileName.substring(fileName.lastIndexOf(File.separator) + 1);
        BufferedOutputStream writer = null;
        try {
            writer = new BufferedOutputStream(new FileOutputStream(directoryPath + File.separator + fileName));
            writer.write(fileContents);
        } catch (IOException e) {
            System.out.println("Error: Unable to write to directory.");
            return null;
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
                //Ignoring this due to location.
            }
        }
        return directoryPath + File.separator + fileName;
    }

    public static String createSubdirectories(String fileName) {
        String subDirectory = "";
        String directoryPath = Utils.getWorkingDirectory().getAbsolutePath();
        while (fileName.contains(File.separator)) {
            subDirectory = fileName.substring(0, fileName.indexOf(File.separator));
            fileName = fileName.substring(subDirectory.length() + 1);
            createDirectory(directoryPath, subDirectory);
            directoryPath = directoryPath + File.separator + subDirectory;
        }
        return directoryPath;
    }

    /**
     * Create's a directory given the paths
     *
     * @param parentDirectory Parent directory to make the new folder in.
     * @param childDirectory  Child directory to create
     */
    public static void createDirectory(String parentDirectory, String childDirectory) {
        File path = new File(parentDirectory, childDirectory);
        path.mkdir();
    }

    /**
     * Gets a list of all files in a given directory and subdirectories, that satisfy the predicate.
     *
     * @param directory File representing the directory to begin the search
     * @param filter Predicate<File> filter used to determine whether or not the file should be added to the list
     * @return List<File> List of files found that satisfy the predicate
     */
    public static List<File> getFilesInDirectory(File directory, Predicate<File> filter) {
        final ArrayList files = new ArrayList();
        try {
            Files.walkFileTree(directory.toPath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
                    File file = path.toFile();
                    if (filter.test(file)) {
                        files.add(file);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return files;
    }
}

package scriptpacker.swingcomponents.frames;

import scriptpacker.swingcomponents.constants.Colors;
import scriptpacker.swingcomponents.panels.header.HeaderPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class HelpFrame extends BaseGuiFrame {

    private JPanel contentPane;
    private JScrollPane scrollPane;
    private JTextPane htmlTextPane;
    private String htmlText = "";

    public HelpFrame(String titleText) {
        super(titleText, 300, 500);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        contentPane.setBackground(Colors.DarkTheme.BACKGROUND.getColor());
        setContentPane(contentPane);

        HeaderPanel headerPanel = new HeaderPanel("Help/FAQ");
        headerPanel.setHelpVisible(false);
        contentPane.add(headerPanel, BorderLayout.NORTH);

        scrollPane = new JScrollPane();
        scrollPane.setBorder(null);
        contentPane.add(scrollPane, BorderLayout.CENTER);

        htmlTextPane = new JTextPane();
        htmlTextPane.setBackground(Colors.DarkTheme.FIELD_BACKGROUND.getColor());
        htmlTextPane.setContentType("text/html");
        htmlTextPane.setForeground(Colors.DarkTheme.FIELD_TEXT_COLOR.getColor());
        htmlTextPane.setText(htmlText);
        htmlTextPane.setEditable(false);
        scrollPane.setViewportView(htmlTextPane);
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    @Override
    public void paintComponents(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        super.paintComponents(g);
    }
}

package scriptpacker.swingcomponents.frames;

import scriptpacker.swingcomponents.ComponentMover;
import scriptpacker.swingcomponents.constants.Colors;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

public class BaseGuiFrame extends JFrame {
	
	private static final long serialVersionUID = -5132474799829962838L;

	public BaseGuiFrame(String titleText) {
		this(titleText, 800, 600);
	}

	public BaseGuiFrame (String titleText, int width, int height) {
		this.setTitle(titleText);
		this.setBounds(0, 0, width, height);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.setUndecorated(true);
		this.setShape(new RoundRectangle2D.Double(0, 0, width, height, 20, 20));
		this.getContentPane().setBackground(Colors.DarkTheme.BACKGROUND.getColor());
		this.setBackground(Colors.DarkTheme.BACKGROUND.getColor());
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		ComponentMover componentMover = new ComponentMover();
		componentMover.registerComponent(this);
	}

	@Override
	public void paintComponents(Graphics g) {
		Graphics2D graphics2D = (Graphics2D) g;
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		super.paintComponents(g);
	}
}

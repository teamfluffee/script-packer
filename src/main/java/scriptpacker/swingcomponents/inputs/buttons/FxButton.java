package scriptpacker.swingcomponents.inputs.buttons;

import scriptpacker.swingcomponents.constants.Colors;
import scriptpacker.swingcomponents.swingmaterialdesign.MaterialButton;

import java.awt.*;

public class FxButton extends MaterialButton {

    private String buttonText;

    public FxButton(String buttonText) {
        super(buttonText);
        this.setBackground(Colors.DarkTheme.BUTTON_COLOR.getColor());
        this.setFont(new Font("Roboto", Font.PLAIN, 14));
        this.setBorderRadius(8);
        this.setPreferredSize(new Dimension(100, 50));
    }
}

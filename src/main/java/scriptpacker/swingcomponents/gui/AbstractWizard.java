package scriptpacker.swingcomponents.gui;

import scriptpacker.swingcomponents.frames.BaseGuiFrame;
import scriptpacker.swingcomponents.panels.BottomButtonPane;
import scriptpacker.swingcomponents.panels.PanelSlider;
import scriptpacker.swingcomponents.panels.header.CurrentStepPane;

import javax.swing.*;

class AbstractWizard {
    protected CurrentStepPane pnlStepTracker;
    protected BottomButtonPane pnlBottomButtons;
    protected AbstractWizardStep[] steps;
    protected PanelSlider<JFrame> slider;
    protected BaseGuiFrame baseGuiFrame;
    protected int currentStep;
    protected PanelSlider.AnimationFinishedListener currentStepUpdateListener;

    AbstractWizard() {
        currentStepUpdateListener = currentIndex -> currentStep = currentIndex;
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }

    public PanelSlider.AnimationFinishedListener getCurrentStepUpdateListener() {
        return currentStepUpdateListener;
    }
}

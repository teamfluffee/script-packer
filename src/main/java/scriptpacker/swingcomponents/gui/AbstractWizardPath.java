package scriptpacker.swingcomponents.gui;

import java.awt.*;

public abstract class AbstractWizardPath extends AbstractWizard {
    protected abstract int getNumberSteps();
    protected abstract String[] getStepNames();
    protected abstract Color getStepButtonBackground();
    protected abstract AbstractWizardStep[] getSteps();
    protected String getLeftButtonText() {
        return "Previous";
    }
    protected String getRightButtonText() {
        return "Next";
    }
    protected boolean getLeftButtonVisible() {
        return false; //False because on start we probably don't want to be able to go back a step.
    }
    protected boolean getRightButtonVisible() {
        return true;
    }
}

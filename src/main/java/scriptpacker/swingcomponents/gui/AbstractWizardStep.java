package scriptpacker.swingcomponents.gui;

import scriptpacker.swingcomponents.panels.ColumnPanel;
import scriptpacker.swingcomponents.panels.StepPanel;

public abstract class AbstractWizardStep extends AbstractWizard {

    private StepPanel stepPanel;
    private ColumnPanel columnPanel;

    protected abstract void setupPane();
    protected boolean isStepComplete() {return true;}
    protected void onStepCompletion() {};
    protected void onAnimationInFinished() {};
    protected void onAnimationOutFinished() {};

    public StepPanel getStepPanel() {
        return stepPanel;
    }

    public void setStepPanel(StepPanel stepPanel) {
        this.stepPanel = stepPanel;
    }

    public ColumnPanel getColumnPanel() {
        return columnPanel;
    }

    public void setColumnPanel(ColumnPanel columnPanel) {
        this.columnPanel = columnPanel;
    }
}

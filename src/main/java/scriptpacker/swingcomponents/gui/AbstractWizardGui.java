package scriptpacker.swingcomponents.gui;

import scriptpacker.swingcomponents.fonts.FontUtilities;
import scriptpacker.swingcomponents.frames.BaseGuiFrame;
import scriptpacker.swingcomponents.frames.HelpFrame;
import scriptpacker.swingcomponents.jiconfont.icons.FontAwesome;
import scriptpacker.swingcomponents.jiconfont.swing.IconFontSwing;
import scriptpacker.swingcomponents.panels.BottomButtonPane;
import scriptpacker.swingcomponents.panels.PanelSlider;
import scriptpacker.swingcomponents.panels.header.CurrentStepPane;
import scriptpacker.swingcomponents.panels.header.HeaderPanel;
import scriptpacker.utilities.Utils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public abstract class AbstractWizardGui extends AbstractWizard {

    protected HeaderPanel pnlHeader;

    public AbstractWizardGui(String frameTitle, String description) {
        IconFontSwing.register(FontAwesome.getIconFont());
        FontUtilities.createFontAwesomeFont("FluffeeScripts" + File.separator + "gui" + File.separator + "fontawesome-webfont.ttf");
        FontUtilities.createRobotoFont("FluffeeScripts" + File.separator + "gui" + File.separator + "Roboto.otf");
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(
                    Utils.getFluffeeScriptsDirectory() + "gui" + File.separator + "fontawesome-webfont.ttf")));
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(
                    Utils.getFluffeeScriptsDirectory() + "gui" + File.separator + "Roboto.otf")));
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }

        JFrame.setDefaultLookAndFeelDecorated(false);
        baseGuiFrame = new BaseGuiFrame(frameTitle);
        slider = new PanelSlider<>(baseGuiFrame);
        baseGuiFrame.getContentPane().add(slider.getBasePanel(), BorderLayout.CENTER);
        pnlHeader = new HeaderPanel(frameTitle,
                description, 10);
        pnlStepTracker = CurrentStepPane.createHiddenPane();
        pnlHeader.add(pnlStepTracker, BorderLayout.SOUTH);

        pnlBottomButtons = new BottomButtonPane();
        pnlBottomButtons.setVisible(false);

        baseGuiFrame.getContentPane().add(pnlHeader, BorderLayout.NORTH);
        baseGuiFrame.getContentPane().add(pnlBottomButtons, BorderLayout.SOUTH);

    }

    public void show() {
        baseGuiFrame.setVisible(true);
    }

    public boolean isOpen() {
        return baseGuiFrame.isVisible();
    }

    public void close() {
        baseGuiFrame.dispose();
    }

    public void setHelpFrame(HelpFrame helpFrame) {
        this.pnlHeader.setHelpFrame(helpFrame);
    }

    public void setupStepTrackerListeners() {
        if (steps.length != pnlStepTracker.getNumberSteps()) {
            return;
        }
        for (int i = 0; i < steps.length; i++) {
            final int index = i;
            pnlStepTracker.addButtonActionListener(index, e -> {
                if (slider.getCurrentPanelIndex() == index || !steps[currentStep].isStepComplete()) {
                    e.setSource(null);
                    pnlStepTracker.setActiveButtonIndex(0);
                    return;
                } else if (slider.getCurrentPanelIndex() < index) {
                    steps[index].onStepCompletion();
                    slider.slideLeft(index);
                } else {
                    slider.slideRight(index);
                }
            });
        }
    }

    public void setGuiPath(AbstractWizardPath path) {
        pnlStepTracker.setNumberSteps(path.getNumberSteps());
        pnlStepTracker.setActiveButtonBackground(path.getStepButtonBackground());
        pnlStepTracker.setHeaderButtons(path.getStepNames());

        pnlBottomButtons.setLeftButton(path.getLeftButtonText());
        pnlBottomButtons.setRightButton(path.getRightButtonText());
        pnlBottomButtons.setLeftButtonVisible(path.getLeftButtonVisible());
        pnlBottomButtons.setRightButtonVisible(path.getRightButtonVisible());

        steps = path.getSteps();

        for (int i = 0; i < steps.length; i++) {
            steps[i].setupPane();
            final int index = i;
            if (i == 0) {
                steps[i].getStepPanel().setAnimationFinishedListener(new PanelSlider.AnimationFinishedListener() {
                    @Override
                    public void onAnimationInFinished(int currentPanelIndex) {
                        pnlBottomButtons.setVisible(true);
                        pnlBottomButtons.setLeftButtonVisible(false);
                        pnlBottomButtons.setRightButtonVisible(true);
                        pnlBottomButtons.setRightButtonActionListener(e -> {
                            if (steps[currentStep].isStepComplete()) {
                                steps[currentStep].onStepCompletion();
                                slider.slideLeft();
                            }
                        });
                        pnlStepTracker.setActiveButtonIndex(index);
                        steps[currentStep].onAnimationInFinished();
                    }

                    @Override
                    public void onAnimationOutFinished() {
                        pnlBottomButtons.setLeftButtonVisible(true);
                        steps[currentStep].onAnimationOutFinished();
                    }
                });
            } else if (i == steps.length - 1) {
                steps[i].getStepPanel().setAnimationFinishedListener(new PanelSlider.AnimationFinishedListener() {
                    @Override
                    public void onAnimationInFinished(int currentPanelIndex) {
                        pnlBottomButtons.setRightButtonText("Start!");
                        pnlBottomButtons.setRightButtonActionListener(e -> {
                            if (steps[currentStep].isStepComplete()) {
                                steps[currentStep].onStepCompletion();
                                close();
                            }
                        });
                        pnlBottomButtons.setLeftButtonActionListener(e -> {
                            if (steps[currentStep].isStepComplete()) {
                                slider.slideRight();
                            }
                        });
                        pnlStepTracker.setActiveButtonIndex(index);
                        steps[currentStep].onAnimationInFinished();
                    }

                    @Override
                    public void onAnimationOutFinished() {
                        pnlBottomButtons.setRightButtonText("Next");
                        steps[currentStep].onAnimationOutFinished();
                    }
                });
            } else {
                steps[i].getStepPanel().setAnimationFinishedListener(new PanelSlider.AnimationFinishedListener() {
                    @Override
                    public void onAnimationInFinished(int currentPanelIndex) {
                        pnlBottomButtons.setRightButtonActionListener(e -> {
                            if (steps[currentStep].isStepComplete()) {
                                steps[currentStep].onStepCompletion();
                                slider.slideLeft();
                            }
                        });
                        pnlBottomButtons.setLeftButtonActionListener(e -> {
                            if (steps[currentStep].isStepComplete()) {
                                slider.slideRight();
                            }
                        });
                        pnlStepTracker.setActiveButtonIndex(index);
                        steps[currentStep].onAnimationInFinished();
                    }

                    @Override
                    public void onAnimationOutFinished() {
                        steps[currentStep].onAnimationOutFinished();
                    }
                });
            }
            steps[i].getStepPanel().setPathListener(getCurrentStepUpdateListener());
            steps[i].getStepPanel().setPanelContents(steps[i].getColumnPanel());
            slider.addComponent(steps[i].getStepPanel());
        }
        setupStepTrackerListeners();
    }

    protected abstract void setupStartButtonPane();

}

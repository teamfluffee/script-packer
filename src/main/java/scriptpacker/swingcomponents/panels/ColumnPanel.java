package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;

import javax.swing.border.MatteBorder;
import java.awt.*;

public abstract class ColumnPanel extends BasePanel {

    final int twoColumnWidth = 275;
    final int singleColumnWidth = 350;
    final int spacerColumnWidth = 30;
    GridBagLayout columnLayout;

    ColumnPanel() {
        super();
        this.setBorder(new MatteBorder(0, 10, 15, 0, borderColor));
        this.setViewportChangeListener(e -> { //Used to center the viewport contents if the scrollbar becomes visible.
            if (getVerticalScrollBar().getWidth() != this.getScrollbarWidth()) {
                this.setScrollbarWidth(getVerticalScrollBar().getWidth());
                setBorder(new MatteBorder(0, scrollbarWidth, 15, 0, borderColor));
            }
        });
        columnLayout = new GridBagLayout();
    }

    @Override
    public Component add(Component comp) {
        if (comp instanceof InputPanel) {
            addInputPanel((InputPanel) comp);
        } else {
            super.add(comp);
        }
        return comp;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    public abstract void addInputPanel(InputPanel inputPanel);
}

package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;
import scriptpacker.swingcomponents.inputs.FxList;

import javax.swing.*;
import java.awt.*;

public class FxListPanel extends JPanel {

    private FxList fxList;
    private BasePanel listScrollPanel;
    private JPanel buttonPanel;

    public FxListPanel(FxList fxList) {
        this.fxList = fxList;
        this.buttonPanel = new JPanel();
        this.listScrollPanel = new BasePanel();

        this.listScrollPanel.setViewportView(this.fxList);
        this.buttonPanel.setLayout(new FlowLayout());
        this.buttonPanel.setOpaque(false);

        this.setOpaque(false);
        this.setLayout(new BorderLayout());
        this.add(this.listScrollPanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }

}

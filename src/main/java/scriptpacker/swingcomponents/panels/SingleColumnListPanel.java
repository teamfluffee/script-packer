package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;
import scriptpacker.swingcomponents.inputs.FxList;

import java.awt.*;

public class SingleColumnListPanel extends ColumnPanel {

    private final int[] columnWidths = new int[] {0, singleColumnWidth, 0};
    private final double[] columnWeights = new double[] {endColumnWeight, innerColumnWeight, endColumnWeight};
    private FxListPanel listPanel;
    private GridBagConstraints listPanelConstraints;

    public SingleColumnListPanel() {
        super();

        columnLayout.rowHeights = new int[]{0};
        columnLayout.rowWeights = new double[]{Double.MIN_VALUE};
        columnLayout.columnWidths = columnWidths;
        columnLayout.columnWeights = columnWeights;
        innerPane.setLayout(columnLayout);
    }

    public void setList(FxList list) {
        setListPanel(new FxListPanel(list));
    }

    public void setListPanel(FxListPanel listPanel) {
        this.listPanel = listPanel;

        listPanelConstraints = new GridBagConstraints();
        listPanelConstraints.fill = GridBagConstraints.BOTH;
        listPanelConstraints.gridy = 0;
        listPanelConstraints.gridx = 1;
        innerPane.add(this.listPanel, listPanelConstraints);
    }

    public void addInputPanel(InputPanel inputPanel) {
        return;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }

}
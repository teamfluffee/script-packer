package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class TwoColumnPanel extends ColumnPanel {

	private JPanel flowLayout;
	private InputPanel lastSinglePanel;
	private GridBagConstraints flowLayoutConstraints;
	private final int[] columnWidths = new int[] {0, twoColumnWidth, spacerColumnWidth, twoColumnWidth, 0};
	private final double[] columnWeights = new double[] {endColumnWeight, innerColumnWeight, innerColumnWeight,
			innerColumnWeight, endColumnWeight}; //The end columns get a higher weight so they can grow.

	public TwoColumnPanel() {
		super();

		columnLayout.rowHeights = new int[]{0};
		columnLayout.rowWeights = new double[]{Double.MIN_VALUE};
		columnLayout.columnWidths = columnWidths;
		columnLayout.columnWeights = columnWeights;

		flowLayoutConstraints = new GridBagConstraints();
		flowLayoutConstraints.fill = GridBagConstraints.BOTH;
		flowLayoutConstraints.gridwidth = 3;
		flowLayoutConstraints.gridx = 1;
	}
	
	public void addInputPanel(InputPanel inputPanel) {
		numberInputs += 1;
		if (numberInputs % 2 != 0) { //We need to add a new row.
			lastSinglePanel = inputPanel;
			int[] rowHeights = new int[(numberInputs + 1) / 2 + 1];
			double[] rowWeights = new double[(numberInputs + 1) / 2 + 1];
			Arrays.fill(rowHeights, rowHeight);
			Arrays.fill(rowWeights, rowWeight);
			rowHeights[rowHeights.length-1] = 0;
			rowWeights[rowWeights.length-1] = 1.0;
			columnLayout.rowHeights = rowHeights;
			columnLayout.rowWeights = rowWeights;
			
			flowLayout = new JPanel();
			flowLayout.setOpaque(false);
			flowLayoutConstraints.gridy = (numberInputs / 2);
			flowLayout.add(lastSinglePanel);
			innerPane.add(flowLayout, flowLayoutConstraints);
			
		} else {
			innerPane.remove(flowLayout);
			flowLayout = null;
			
			GridBagConstraints existingInputConstraints = new GridBagConstraints();
			existingInputConstraints.fill = GridBagConstraints.BOTH;
			existingInputConstraints.gridx = 1;
			existingInputConstraints.gridy = (numberInputs/2) -1;
			
			GridBagConstraints newInputConstraints = new GridBagConstraints();
			newInputConstraints.fill = GridBagConstraints.BOTH;
			newInputConstraints.gridx = 3;
			newInputConstraints.gridy = (numberInputs/2) -1;

			innerPane.add(lastSinglePanel, existingInputConstraints);
			innerPane.add(inputPanel, newInputConstraints);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}

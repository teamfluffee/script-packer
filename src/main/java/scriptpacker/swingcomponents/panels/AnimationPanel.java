package scriptpacker.swingcomponents.panels;

import javax.swing.*;

public class AnimationPanel extends JPanel {

    private PanelSlider.AnimationFinishedListener animationFinishedListener;

    public AnimationPanel() {
        this.animationFinishedListener = null;
    }

    public PanelSlider.AnimationFinishedListener getAnimationFinishedListener() {
        return animationFinishedListener;
    }

    public void setAnimationFinishedListener(PanelSlider.AnimationFinishedListener animationFinishedListener) {
        this.animationFinishedListener = animationFinishedListener;
    }
}

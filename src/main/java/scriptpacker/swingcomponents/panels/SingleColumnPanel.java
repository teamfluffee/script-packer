package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;

import java.awt.*;
import java.util.Arrays;

public class SingleColumnPanel extends ColumnPanel {

	private final int[] columnWidths = new int[] {0, singleColumnWidth, 0};
	private final double[] columnWeights = new double[] {endColumnWeight, innerColumnWeight, endColumnWeight};

	public SingleColumnPanel() {
		super();

		columnLayout.rowHeights = new int[]{0};
		columnLayout.rowWeights = new double[]{Double.MIN_VALUE};
		columnLayout.columnWidths = columnWidths;
		columnLayout.columnWeights = columnWeights;

		innerPane.setLayout(columnLayout);
	}

	public void addInputPanel(InputPanel inputPanel) {
		numberInputs += 1;

		int[] rowHeights = new int[numberInputs + 1];
		double[] rowWeights = new double[numberInputs + 1];
		Arrays.fill(rowHeights, rowHeight);
		Arrays.fill(rowWeights, rowWeight);
		rowHeights[rowHeights.length-1] = 0;
		rowWeights[rowWeights.length-1] = 1.0;
		columnLayout.rowHeights = rowHeights;
		columnLayout.rowWeights = rowWeights;

		GridBagConstraints inputConstraints = new GridBagConstraints();
		inputConstraints.fill = GridBagConstraints.BOTH;
		inputConstraints.insets = new Insets(0, 0, 5, 5);
		inputConstraints.gridx = 1;
		inputConstraints.gridy = numberInputs - 1;

		innerPane.add(inputPanel, inputConstraints);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}

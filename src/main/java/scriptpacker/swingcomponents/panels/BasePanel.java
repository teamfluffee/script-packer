package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;

public class BasePanel extends JScrollPane {

    int numberInputs = 0;
    InnerPane innerPane;
    final int rowHeight = 65;
    final double rowWeight = 0.0;
    final double endColumnWeight = 1.0; //The end columns get a higher weight so they can grow.
    final double innerColumnWeight = 0.0;
    protected int scrollbarWidth = 0;
    protected Color borderColor;
    private ChangeListener viewportChangeListener;

    public BasePanel() {
        this(Colors.DarkTheme.BACKGROUND.getColor());
    }

    public BasePanel(Color borderColor) {
        this.borderColor = borderColor;
        innerPane = new InnerPane();
        innerPane.setBackground(Colors.DarkTheme.BACKGROUND.getColor());
        this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.setBorder(new MatteBorder(0, 10, 0, 0, borderColor));
        this.setBackground(Colors.DarkTheme.BACKGROUND.getColor());
        this.setViewportView(innerPane);
        this.getVerticalScrollBar().setUI(new FxScrollbarUI());
        this.setViewportChangeListener(e -> { //Used to center the viewport contents if the scrollbar becomes visible.
            if (getVerticalScrollBar().getWidth() != this.getScrollbarWidth()) {
                this.setScrollbarWidth(getVerticalScrollBar().getWidth());
                setBorder(new MatteBorder(0, scrollbarWidth, 0, 0, borderColor));
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        super.paintComponent(g);
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    public int getScrollbarWidth() {
        return scrollbarWidth;
    }

    public void setScrollbarWidth(int scrollbarWidth) {
        this.scrollbarWidth = scrollbarWidth;
    }

    public ChangeListener getViewportChangeListener() {
        return viewportChangeListener;
    }

    public void setViewportChangeListener(ChangeListener viewportChangeListener) {
        this.getViewport().removeChangeListener(this.getViewportChangeListener());
        this.viewportChangeListener = viewportChangeListener;
        this.getViewport().addChangeListener(this.getViewportChangeListener());

    }
}

class InnerPane extends JPanel {
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}

class FxScrollbarUI extends BasicScrollBarUI {
    private final Dimension noButtonDimension = new Dimension();

    @Override
    protected JButton createDecreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return noButtonDimension;
            }
        };
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return noButtonDimension;
            }
        };
    }

    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(r.x, r.y, r.width, r.height + 100);
    }

    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        Color color = null;
        JScrollBar sb = (JScrollBar) c;
        if (!sb.isEnabled() || r.width > r.height) {
            return;
        } else if (isThumbRollover()) {
            color = Colors.DarkTheme.SCROLL_BAR_HOVER_COLOR.getColor();
        } else {
            color = Colors.DarkTheme.FIELD_TEXT_COLOR.getColor();
        }
        g2.setPaint(color);
        g2.fillRoundRect(r.x + r.width / 4, r.y + 6, r.width / 2, r.height - 12, 10, 10);
        g2.setPaint(Colors.DarkTheme.SCROLL_BAR_BORDER_COLOR.getColor());
        g2.drawRoundRect(r.x + r.width / 4, r.y + 6, r.width / 2, r.height - 12, 10, 10);
        g2.dispose();
    }

    @Override
    protected void setThumbBounds(int x, int y, int width, int height) {
        super.setThumbBounds(x, y, width, height);
        scrollbar.repaint();
    }
}

package scriptpacker.swingcomponents.panels;

import scriptpacker.swingcomponents.constants.Colors;
import scriptpacker.swingcomponents.inputs.FxList;

import java.awt.*;
import java.util.Arrays;

public class TwoColumnListPanel extends ColumnPanel {

    private final int[] columnWidths = new int[] {0, singleColumnWidth, spacerColumnWidth, twoColumnWidth, 0};
    private final double[] columnWeights = new double[] {endColumnWeight, innerColumnWeight, innerColumnWeight,
            innerColumnWeight, endColumnWeight}; //The end columns get a higher weight so they can grow.
    private FxListPanel listPanel;
    private GridBagConstraints listPanelConstraints;
    private GridBagLayout masterInputLayout;
    private BasePanel masterInputPanel;

    public TwoColumnListPanel() {
        super();

        columnLayout.rowHeights = new int[]{0};
        columnLayout.rowWeights = new double[]{Double.MIN_VALUE};
        columnLayout.columnWidths = columnWidths;
        columnLayout.columnWeights = columnWeights;

        innerPane.setLayout(columnLayout);

        masterInputPanel = new BasePanel();
        masterInputPanel.setOpaque(false);
        masterInputLayout = new GridBagLayout();
        masterInputLayout.rowHeights = new int[]{0};
        masterInputLayout.rowWeights = new double[]{Double.MIN_VALUE};
        masterInputLayout.columnWidths = new int[] {0};
        masterInputLayout.columnWeights= new double[] {1.0};
        masterInputPanel.innerPane.setLayout(masterInputLayout);

        GridBagConstraints masterInputConstraints = new GridBagConstraints();
        masterInputConstraints.fill = GridBagConstraints.BOTH;
        masterInputConstraints.gridy = 0;
        masterInputConstraints.gridx = 3;
        innerPane.add(this.masterInputPanel, masterInputConstraints);
    }

    public void setList(FxList list) {
        setListPanel(new FxListPanel(list));
    }

    public void setListPanel(FxListPanel listPanel) {
        this.listPanel = listPanel;

        listPanelConstraints = new GridBagConstraints();
        listPanelConstraints.fill = GridBagConstraints.BOTH;
        listPanelConstraints.gridy = 0;
        listPanelConstraints.gridx = 1;
        innerPane.add(this.listPanel, listPanelConstraints);
    }

    public void addInputPanel(InputPanel inputPanel) {
        numberInputs += 1;

        int[] rowHeights = new int[numberInputs + 1];
        double[] rowWeights = new double[numberInputs + 1];
        Arrays.fill(rowHeights, rowHeight);
        Arrays.fill(rowWeights, rowWeight);
        rowHeights[rowHeights.length-1] = 0;
        rowWeights[rowWeights.length-1] = 1.0;
        masterInputLayout.rowHeights = rowHeights;
        masterInputLayout.rowWeights = rowWeights;

        GridBagConstraints inputConstraints = new GridBagConstraints();
        inputConstraints.fill = GridBagConstraints.BOTH;
        inputConstraints.insets = new Insets(0, 0, 5, 5);
        inputConstraints.gridx = 0;
        inputConstraints.gridy = numberInputs - 1;

        masterInputPanel.innerPane.add(inputPanel, inputConstraints);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Colors.DarkTheme.BACKGROUND.getColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}

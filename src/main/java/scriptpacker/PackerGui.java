package scriptpacker;

import scriptpacker.swingcomponents.fonts.FontUtilities;
import scriptpacker.swingcomponents.frames.BaseGuiFrame;
import scriptpacker.swingcomponents.inputs.FxList;
import scriptpacker.swingcomponents.inputs.FxTextField;
import scriptpacker.swingcomponents.inputs.buttons.FxButton;
import scriptpacker.swingcomponents.jiconfont.icons.FontAwesome;
import scriptpacker.swingcomponents.jiconfont.swing.IconFontSwing;
import scriptpacker.swingcomponents.panels.InputPanel;
import scriptpacker.swingcomponents.panels.TwoColumnListPanel;
import scriptpacker.swingcomponents.panels.header.HeaderPanel;
import scriptpacker.utilities.ScriptClassFinder;
import scriptpacker.utilities.Utils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import static scriptpacker.Main.PREF_KEY;

public class PackerGui {

    private BaseGuiFrame baseGuiFrame;
    private HeaderPanel pnlHeader;
    private TwoColumnListPanel pnlContents;
    private FxList lstScripts;
    private FxTextField txtSourceDirectory, txtOutputDirectory;
    private FxButton btnPickSourceDirectory, btnPickOutputDirectory, btnFindScripts, btnPackScript;
    private HashMap<String, File> scriptMap;
    private List<File> sourceFiles;
    private ScriptClassFinder scriptClassFinder;
    private Preferences userPref;

    public PackerGui() {
        IconFontSwing.register(FontAwesome.getIconFont());
        FontUtilities.createFontAwesomeFont("FluffeeScripts" + File.separator + "gui" + File.separator + "fontawesome-webfont.ttf");
        FontUtilities.createRobotoFont("FluffeeScripts" + File.separator + "gui" + File.separator + "Roboto.otf");
        userPref =  Preferences.userRoot().node(PREF_KEY);
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(
                    Utils.getFluffeeScriptsDirectory() + "gui" + File.separator + "fontawesome-webfont.ttf")));
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(
                    Utils.getFluffeeScriptsDirectory() + "gui" + File.separator + "Roboto.otf")));
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }

        scriptClassFinder = new ScriptClassFinder();

        JFrame.setDefaultLookAndFeelDecorated(false);
        baseGuiFrame = new BaseGuiFrame("Fluffee's Script Packer");
        pnlHeader = new HeaderPanel("Fluffee's Script Packer",
                "Packs scripts and their dependencies for uploading", 10);

        baseGuiFrame.getContentPane().add(pnlHeader, BorderLayout.NORTH);

        pnlContents = new TwoColumnListPanel();

        lstScripts = new FxList();
        pnlContents.setList(lstScripts);

        txtSourceDirectory = new FxTextField();
        txtSourceDirectory.setText(userPref.get("source", ""));
        pnlContents.addInputPanel(new InputPanel("Source Files Location", txtSourceDirectory));

        btnPickSourceDirectory = new FxButton("Find Source Folder");
        btnPickSourceDirectory.addActionListener((event) -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("."));
            chooser.setDialogTitle("Source File Location");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showOpenDialog(baseGuiFrame) == JFileChooser.APPROVE_OPTION) {
               txtSourceDirectory.setText(chooser.getSelectedFile().toString() + File.separator);
            }
        });
        pnlContents.addInputPanel(new InputPanel("", btnPickSourceDirectory));

        txtOutputDirectory = new FxTextField();
        txtOutputDirectory.setText(userPref.get("output", ""));
        pnlContents.addInputPanel(new InputPanel("Target Output Location", txtOutputDirectory));

        btnPickOutputDirectory = new FxButton("Find Output Folder");
        btnPickOutputDirectory.addActionListener((event) -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("."));
            chooser.setDialogTitle("Output Location");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showOpenDialog(baseGuiFrame) == JFileChooser.APPROVE_OPTION) {
                txtOutputDirectory.setText(chooser.getSelectedFile().toString() + File.separator);
            }
        });
        pnlContents.addInputPanel(new InputPanel("", btnPickOutputDirectory));

        btnFindScripts = new FxButton("Load Scripts");
        btnFindScripts.addActionListener(e -> {
            if (txtSourceDirectory.getText().isEmpty()) {
                return;
            }
            File sourceDirectory = new File(txtSourceDirectory.getText());

            if (!sourceDirectory.exists() || !sourceDirectory.isDirectory()) {
                return;
            }

            userPref.put("source", txtSourceDirectory.getText());
            sourceFiles = Utils.getFilesInDirectory(sourceDirectory,
                    (file) -> file.getName().contains("."));
            List<File> classFiles = Utils.getFilesInDirectory(Utils.getBinDirectory(),
                    (file) -> file.getName().endsWith(".class"));
            try {

                scriptMap = scriptClassFinder.findScriptClasses(classFiles);
                lstScripts.getListModel().removeAllElements();
                for (Map.Entry<String, File> entry : scriptMap.entrySet()) {
                    lstScripts.addItem(entry.getKey());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        pnlContents.addInputPanel(new InputPanel("", btnFindScripts));

        btnPackScript = new FxButton("Pack Script");
        btnPackScript.addActionListener(e -> {
            int selectedIndex = lstScripts.getSelectedIndex();
            if (selectedIndex == -1 || txtOutputDirectory.getText().isEmpty()) {
                return;
            }
            File outputDirectory = new File(txtOutputDirectory.getText());
            if (!outputDirectory.exists() || !outputDirectory.isDirectory()) {
                return;
            }

            userPref.put("output", txtOutputDirectory.getText());

            ScriptPacker scriptPacker = new ScriptPacker();
            try {
                String scriptName = (String) (lstScripts.getSelectedValue());
                File scriptFile = scriptMap.get(lstScripts.getSelectedValue());
                scriptPacker.packScript(scriptName, outputDirectory, scriptFile, sourceFiles);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        pnlContents.addInputPanel(new InputPanel("", btnPackScript));

        baseGuiFrame.getContentPane().add(pnlContents, BorderLayout.CENTER);
        baseGuiFrame.setVisible(true);

    }
}

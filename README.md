# script-packer

A script packer for TRiBot, forked from AlphaDog&#39;s script packer. Reads the compiled class files and creates a single zip with the script and all of it&#39;s required dependencies.